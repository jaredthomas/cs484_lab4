#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>
// 
// #define SIZE        1024;
// #define EPSILON     0.1;

double When();

int main(int argc, char *argv[])
{
    double tic, toc;
    tic = When();

    int i, j, it=0, done;     
    float **nA, **oA, **tmp;   
    int numtasks, taskid;
    float error, max_error=1., realMAX=1.;
    int SIZE = 16384;
    float EPSILON = 0.1;

    nA = (float**)malloc((SIZE)*sizeof(float*));
    oA = (float**)malloc((SIZE)*sizeof(float*));
    fprintf(stderr, "Line: %d\n",__LINE__);

    #pragma omp parallel for
    for (i=0; i<SIZE; i++){
        nA[i] = (float*)malloc(SIZE*sizeof(float));
        oA[i] = (float*)malloc(SIZE*sizeof(float));
// 		fprintf(stderr, "task %d, nA[%d]: %x\n", taskid, i, nA[i]);
// 		fprintf(stderr, "task %d, oA[%d]: %x\n", taskid, i, oA[i]);
		
    }
    
//     fprintf(stderr, "taskid %d allocated arrays\n", taskid);
    #pragma omp parallel for
    /* initialize all cells */
    for (i=0; i<SIZE; i++){
        for (j=0; j<SIZE; j++){
            nA[i][j] = oA[i][j] = 50.;
        }
    }
    
//     fprintf(stderr, "taskid %d initialized all cells to 50\n", taskid);
    
    /* initialize boundaries */
    #pragma omp parallel for
    for (j=0; j<SIZE; j++){
        nA[0][j] = oA[0][j] = 0.;
        nA[SIZE-1][j] = oA[SIZE-1][j] = 100.;
    }	
    
    #pragma omp parallel for
	for (i=0;i<SIZE;i++){
		nA[i][0] = oA[i][0] = nA[i][SIZE-1] = oA[i][SIZE-1] = 0.;
	}    
    
    /*// check allocation  (obsolete)   
    for (i = 0; i<size; i++){
        for (j = 0; j<size; j++){
            if (heat1[i][j] == 100. && i == 400){
                printf("%i \n", j);
                printf("%f \n", heat1[i][j]);
            }
         }
     }*/
     
     // calculate steady state
     while(realMAX > EPSILON) {
        /* update iteration counter */
        it ++;
        
        /* swap pointer addresses */
        tmp = nA;
        nA = oA;
        oA = tmp;
      
        // update temps for current time step
        #pragma omp parallel for
        for (i = 1; i<SIZE-1; i++){
            for (j = 1; j<SIZE-1; j++){
// 				fprintf(stderr, "nA[%d] = %x, oA[%d] = %x, taskid: %d\n", i, nA[i], i+1, oA[i+1], taskid);
                nA[i][j] = (oA[i+1][j] + oA[i-1][j] + oA[i][j+1] + oA[i][j-1] + 4*oA[i][j])/8.;
            }
        }  
// 		fprintf(stderr, "Line %d, task %d\n", __LINE__, taskid);
        /* check if we are done */
        for (i = 1; i<SIZE-1; i++){
            max_error = 0.;
//             fprintf(stderr, "task: %d, i = %i, j = %i \n", taskid, i, j);
            for (j = 1; j<SIZE-1; j++){       
                error = fabs(nA[i][j] - (nA[i+1][j] + nA[i-1][j] + nA[i][j+1] + nA[i][j-1])/4);
                if (error > max_error){
                    max_error = error;
                    // printf("i = %i, j = %i \n", i, j);
                    // printf("%f, %f, %f, %f %f \n", heat2[i][j], heat2[i+1][j], heat2[i-1][j], heat2[i][j+1], heat2[i][j-1]);
                    // break;
                } 
                if (max_error >= 0.1){
                    break;
                }                   
            }
            if (max_error >= 0.1){
                break;
                
            }
        } 
//         fprintf(stderr, "Line %d, task %d\n", __LINE__, taskid);
//         MPI_Allreduce(&max_error, &realMAX, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD);        
//         fprintf(stderr, "Line %d, task %d\n", __LINE__, taskid);
        realMAX = max_error;
        fprintf(stderr, "iteration: %i \n", it);   
        fprintf(stderr, "task: %d, realMAX(error) = %f \n", taskid, realMAX);
        //printf("i = %i, j = %i \n", i, j);
        //printf("%f, %f, %f, %f %f \n", heat2[i][j], heat2[i+1][j], heat2[i-1][j], heat2[i][j+1], heat2[i][j-1]);       
     }

     for (i = 0; i<SIZE; i++){
            free(nA[i]);
            free(oA[i]);
     }
 
    printf("iterations: %i \n", it);
    printf("realMAX error: %f \n", realMAX);
    toc = When();     
    printf("program completed \n");
    printf("execution time = %f s \n", toc-tic);
// 	 MPI_Barrier(MPI_COMM_WORLD);
// 	 MPI_Finalize();
	 
     return 0;
}

/* Return the current time in seconds, using a double precision number. */
double When()
{
struct timeval tp;
gettimeofday(&tp, NULL);
return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}