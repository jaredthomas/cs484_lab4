#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>

int main()
{
    double tic, toc, When();
    
    tic = When();
    
    printf("entering program \n");
    
    int size=16384, i, j, it=0;    
    static float heat1[16384][16384], heat2[16384][16384];
    double error, max_error=0.2;

    // Initialize heat arrays
    #pragma omp parallel for
    for (i=0; i<size; i++){
        //printf("i = %i \n", i);
        
        // set top rows vals to 0
        if (i == 0){
            for (j = 0; j<size; j++){
                //heat1[i][j] = 0.;
                heat2[i][j] = 0.;
            }
        }
        
        // set bottom row vals to 100
        else if (i == size-1){
            for (j = 0; j<size; j++){
                //heat1[i][j] = 100.;
                heat2[i][j] = 100.;
            }
        }
        
        // initialize the values for rest of array
        else{
            for (j = 0; j<size; j++){
                if (j == 0 || j == size-1){
                    //heat1[i][j] = 0.;
                    heat2[i][j] = 0.;
                }
                else{
                    //heat1[i][j] = 50.;
                    heat2[i][j] = 50.;
                }
            }
        }
        
         
    }
    
    
    /*// check allocation     
    for (i = 0; i<size; i++){
        for (j = 0; j<size; j++){
            if (heat1[i][j] == 100. && i == 400){
                printf("%i \n", j);
                printf("%f \n", heat1[i][j]);
            }
         }
     }*/
     
     // calculate steady state
     while(max_error >= 0.1) {
        it ++;
        
        // set arrays for next time-step
        for (i = 0; i<size; i++){
            for (j = 1; j<size-1; j++){
                heat1[i][j] = heat2[i][j];
                }
        }    
                
        // update temps for current time step
        #pragma omp parallel for
        for (i = 1; i<size-1; i++){
            for (j = 1; j<size-1; j++){
                heat2[i][j] = (heat1[i+1][j] + heat1[i-1][j] + heat1[i][j+1] + heat1[i][j-1] + 4*heat1[i][j])/8;
            }
        }  
    
        for (i = 1; i<size-1; i++){
            max_error = 0.;
            // printf("i = %i, j = %i \n", i, j);
            for (j = 1; j<size-1; j++){                
                if ((i == 400 && j <= 330) || (i == 200 && j == 500)){
                    //printf("i = %i, j = %i \n", i, j);
                    continue;
                }                
                
                error = fabs(heat2[i][j] - (heat2[i+1][j] + heat2[i-1][j] + heat2[i][j+1] + heat2[i][j-1])/4);
                if (error > max_error){
                    max_error = error;
                    // printf("i = %i, j = %i \n", i, j);
                    // printf("%f, %f, %f, %f %f \n", heat2[i][j], heat2[i+1][j], heat2[i-1][j], heat2[i][j+1], heat2[i][j-1]);
                    // break;
                } 
                if (max_error >= 0.1){
                    break;
                }                   
            }
            if (max_error >= 0.1){
                break;
            }
        } 
        
        //printf("max_error = %f \n", max_error);
        //printf("iteration: %i \n", it);   
        //printf("i = %i, j = %i \n", i, j);
        //printf("%f, %f, %f, %f %f \n", heat2[i][j], heat2[i+1][j], heat2[i-1][j], heat2[i][j+1], heat2[i][j-1]);
                    
        
     }
     printf("iterations: %i \n", it);
     toc = When();
     
     printf("program completed \n");
     printf("execution time = %f s \n", toc-tic);
     
     return 0;
}

/* Return the current time in seconds, using a double precision number. */
double When()
{
struct timeval tp;
gettimeofday(&tp, NULL);
return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}