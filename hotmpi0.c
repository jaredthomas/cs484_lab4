#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <sys/time.h>
// 
// #define SIZE        1024;
// #define EPSILON     0.1;

double When();

int main(int argc, char *argv[])
{
    double tic, toc;
    tic = When();

    int i, j, it=0, done;     
    float **nA, **oA, **tmp;   
    int start, end;
    int theSize;
    int numtasks, taskid;
    float error, max_error=1., realMAX=1.;
    int SIZE = 16384;
    float EPSILON = 0.1;

    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
    
    fprintf(stderr, "Hello from task %d of %d\n", taskid, numtasks-1);
    
    /* Determine how much each task should be doing and allocate arrays */
    theSize = SIZE/numtasks;
    
//     fprintf(stderr, "taskid %d determined theSize to be %d\n", taskid, theSize);
       
    nA = (float**)malloc((theSize + 2)*sizeof(float*));
    oA = (float**)malloc((theSize + 2)*sizeof(float*));
    
//     fprintf(stderr, "taskid %d initialized pointers\n", taskid);
    
    for (i=0; i<theSize + 2; i++){
        nA[i] = (float*)malloc(theSize*sizeof(float));
        oA[i] = (float*)malloc(theSize*sizeof(float));
// 		fprintf(stderr, "task %d, nA[%d]: %x\n", taskid, i, nA[i]);
// 		fprintf(stderr, "task %d, oA[%d]: %x\n", taskid, i, oA[i]);
		
    }
    
//     fprintf(stderr, "taskid %d allocated arrays\n", taskid);
    
    start = 1;
    end = theSize + 1;
        
    /* initialize all cells */
    for (i=0; i<theSize+2; i++){
        for (j=0; j<theSize; j++){
            nA[i][j] = oA[i][j] = 50.;
        }
    }
    
//     fprintf(stderr, "taskid %d initialized all cells to 50\n", taskid);
    
    /* initialize boundaries */
    if (taskid == 0){
        start = 2;
        for (j=0; j<theSize; j++){
            nA[start-1][j] = oA[start-1][j] = 0.;
        }
    } else if (taskid == numtasks-1){
        end = theSize;
        for (j=0; j<theSize; j++){
            nA[end+1][j] = oA[end+1][j] = 100.;
        }
    }
	
	for (i=0;i<theSize+2;i++){
		nA[i][0] = oA[i][0] = nA[i][theSize] = oA[i][theSize] = 0.;
	}    
    
    /*// check allocation  (obsolete)   
    for (i = 0; i<size; i++){
        for (j = 0; j<size; j++){
            if (heat1[i][j] == 100. && i == 400){
                printf("%i \n", j);
                printf("%f \n", heat1[i][j]);
            }
         }
     }*/
     
     // calculate steady state
     while(realMAX > EPSILON) {
        /* update iteration counter */
        it ++;
        
        /* swap pointer addresses */
        tmp = nA;
        nA = oA;
        oA = tmp;
        
        /* get neighbor values */
// 		fprintf(stderr, "task %d: before sr\n", taskid);
        if (taskid != 0){
            MPI_Send(&oA[1][0], theSize, MPI_FLOAT, taskid-1, 0, MPI_COMM_WORLD);
            MPI_Recv(&oA[0][0], theSize, MPI_FLOAT, taskid-1, 0, MPI_COMM_WORLD, &status);
        }
        if (taskid != numtasks-1){
            MPI_Send(&oA[theSize][0], theSize, MPI_FLOAT, taskid+1, 0, MPI_COMM_WORLD);
            MPI_Recv(&oA[theSize+1][0], theSize, MPI_FLOAT, taskid+1, 0, MPI_COMM_WORLD, &status);
        }        
//         fprintf(stderr, "task %d: after sr\n", taskid);
// 		fprintf(stderr, "start = %d, end = %d, theSize = %d, taskid: %d\n", start, end, theSize,  taskid);
        // update temps for current time step
        for (i = start; i<end; i++){
            for (j = 1; j<theSize-1; j++){
// 				fprintf(stderr, "nA[%d] = %x, oA[%d] = %x, taskid: %d\n", i, nA[i], i+1, oA[i+1], taskid);
                nA[i][j] = (oA[i+1][j] + oA[i-1][j] + oA[i][j+1] + oA[i][j-1] + 4*oA[i][j])/8.;
            }
        }  
// 		fprintf(stderr, "Line %d, task %d\n", __LINE__, taskid);
        /* check if we are done */
        for (i = start; i<end; i++){
            max_error = 0.;
//             fprintf(stderr, "task: %d, i = %i, j = %i \n", taskid, i, j);
            for (j = 1; j<theSize-1; j++){       
                error = fabs(nA[i][j] - (nA[i+1][j] + nA[i-1][j] + nA[i][j+1] + nA[i][j-1])/4);
                if (error > max_error){
                    max_error = error;
                    // printf("i = %i, j = %i \n", i, j);
                    // printf("%f, %f, %f, %f %f \n", heat2[i][j], heat2[i+1][j], heat2[i-1][j], heat2[i][j+1], heat2[i][j-1]);
                    // break;
                } 
                if (max_error >= 0.1){
                    break;
                }                   
            }
            if (max_error >= 0.1){
                break;
                
            }
        } 
//         fprintf(stderr, "Line %d, task %d\n", __LINE__, taskid);
        MPI_Allreduce(&max_error, &realMAX, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD);        
//         fprintf(stderr, "Line %d, task %d\n", __LINE__, taskid);
        if (taskid == 0){
            fprintf(stderr, "iteration: %i \n", it);   
            fprintf(stderr, "task: %d, realMAX(error) = %f \n", taskid, realMAX);
            
        }
        //printf("i = %i, j = %i \n", i, j);
        //printf("%f, %f, %f, %f %f \n", heat2[i][j], heat2[i+1][j], heat2[i-1][j], heat2[i][j+1], heat2[i][j-1]);       
     }

     for (i = 0; i<theSize+2; i++){
            free(nA[i]);
            free(oA[i]);
        }
     
     if (taskid == 0){
        printf("iterations: %i \n", it);
        printf("realMAX error: %f \n", realMAX);
        toc = When();     
        printf("program completed \n");
        printf("execution time = %f s \n", toc-tic);
     }
	 MPI_Barrier(MPI_COMM_WORLD);
	 MPI_Finalize();
	 
     return 0;
}

/* Return the current time in seconds, using a double precision number. */
double When()
{
struct timeval tp;
gettimeofday(&tp, NULL);
return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}